package com.classpath.springbootdemo.controller;

import com.classpath.springbootdemo.model.Order;
import com.classpath.springbootdemo.service.OrderService;
import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.info.Contact;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.util.Map;

@RestController
@RequestMapping("/api/orders")
@RequiredArgsConstructor
@OpenAPIDefinition(
        info = @Info(description = "Order API application",
                     contact = @Contact(
                                    name = "Pradeep",
                                    email = "developer@gmail.com"
                                    ),
                title = "Orders API ",
                version = "1.0"
                    )

)
public class OrderController {
    private final OrderService orderService;
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    @ApiResponses(
            {
                    @ApiResponse(responseCode = "201", description = "saves the order in the database"),
                    @ApiResponse(responseCode = "500", description = "unable to save the order")
            }
    )
    @Parameters(
        @Parameter(
            name = "order",
            required = true,
            description = "Order to be used",
            allowEmptyValue = false
        )
    )
    @Operation(
           method = "save order",
            description = "to save the order",
            deprecated = false,
            responses = {}
    )
    public Order saveOrder(@RequestBody @Valid Order order){
        //bi directional mapping
        order.getLineItems().forEach(lineItem -> lineItem.setOrder(order));
        return this.orderService.saveOrder(order);
    }

    @GetMapping
    @ApiResponses(
            {
                    @ApiResponse(responseCode = "200", description = "fetches the orders in the database"),
                    @ApiResponse(responseCode = "500", description = "unable to save the order")
            }
    )
    public Map<String, Object> fetchOrders(
                        @RequestParam(defaultValue = "1", required = false) int page,
                        @RequestParam(defaultValue = "10", required = false) int records,
                        @RequestParam(defaultValue = "customerEmail", required = false) String sort){
        return this.orderService.fetchOrders(page, records, sort);
    }

    @GetMapping("/{id}")
    public Order fetchOrderByOrderId(@PathVariable("id") long id){
        return this.orderService.fetchOrderById(id);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteOrderById(@PathVariable("id") long orderId){
        this.orderService.deleteOrderById(orderId);
    }

}