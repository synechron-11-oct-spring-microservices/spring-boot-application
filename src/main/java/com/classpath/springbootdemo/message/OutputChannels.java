package com.classpath.springbootdemo.message;

import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;

public interface OutputChannels {

    @Output("outputOrdersChannel1")
    MessageChannel outputOrdersTopicChannel1();

    @Output("outputOrdersChannel2")
    MessageChannel outputOrdersTopicChannel2();

    @Output("outputOrdersChannel3")
    MessageChannel outputOrdersTopicChannel3();

    @Output("outputOrdersChannel4")
    MessageChannel outputOrdersTopicChannel4();
}