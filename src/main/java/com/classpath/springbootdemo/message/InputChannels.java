package com.classpath.springbootdemo.message;

import org.springframework.cloud.stream.annotation.Input;
import org.springframework.messaging.SubscribableChannel;

public interface InputChannels {

    @Input("orderInputChannels")
    SubscribableChannel orderInputChannel();

    @Input("paymentInputChannels")
    SubscribableChannel paymentInputChannel();
}