package com.classpath.springbootdemo.util;

import com.classpath.springbootdemo.model.LineItem;
import com.classpath.springbootdemo.model.Order;
import com.classpath.springbootdemo.repository.OrderRepository;
import com.github.javafaker.Faker;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.Profile;
import org.springframework.data.jpa.repository.query.Procedure;
import org.springframework.stereotype.Component;
import java.util.concurrent.TimeUnit;
import static java.time.ZoneId.systemDefault;
import static java.util.stream.IntStream.range;


@Component
@Profile("dev")
public class BootstrapAppData implements ApplicationListener<ApplicationReadyEvent> {

    private OrderRepository orderRepository;
    private Faker faker;

    public BootstrapAppData(OrderRepository orderRepository, Faker faker){
        this.orderRepository = orderRepository;
        this.faker = faker;
    }

    @Override
    public void onApplicationEvent(ApplicationReadyEvent event) {
        //declarative
        range(1,200).forEach( (index ) -> {
            Order order = Order.builder()
                                .price(faker.number().randomDouble(2, 25_000, 75_000))
                                .date(faker.date().past(4, TimeUnit.DAYS).toInstant().atZone(systemDefault()).toLocalDate())
                                .customerEmail(faker.internet().emailAddress()).build();

            range(2,4).forEach((var) -> {
                LineItem lineItem = LineItem.builder()
                                    .name(faker.company().name()).qty(faker.number().numberBetween(2, 8))
                                    .price(faker.number().randomDouble(2, 25_000, 75_000)).build();
                order.addLineItem(lineItem);
            });
            this.orderRepository.save(order);
        });
    }
}