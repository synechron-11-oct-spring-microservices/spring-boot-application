package com.classpath.springbootdemo.util;

import lombok.RequiredArgsConstructor;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class ValidateApplicationStartup implements ApplicationListener<ApplicationReadyEvent> {

    private final Environment environment;
    @Override
    public void onApplicationEvent(ApplicationReadyEvent event) {
        System.out.println("=== Application is now ready to serve request ==== ");
        System.out.println(" Environment :: "+environment.getProperty("appname"));
    }
}