package com.classpath.springbootdemo.service;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;

@FeignClient("INVENTORYSERVICE")
public interface InventoryFeignClient {
    @PostMapping("/api/inventory")
    ResponseEntity<Integer> updateInventory();
}