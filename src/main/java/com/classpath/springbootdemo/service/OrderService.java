package com.classpath.springbootdemo.service;

import com.classpath.springbootdemo.message.OutputChannels;
import com.classpath.springbootdemo.model.Order;
import com.classpath.springbootdemo.repository.OrderRepository;
import io.github.resilience4j.circuitbreaker.annotation.CircuitBreaker;
import io.github.resilience4j.retry.annotation.Retry;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.cloud.stream.messaging.Source;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;

import java.time.LocalDate;
import java.util.*;

@Service
@RequiredArgsConstructor
@Slf4j
public class OrderService {

    private final OrderRepository orderRepository;
    private final RestTemplate restTemplate;

    private final OutputChannels outputChannels;
    private final DiscoveryClient discoveryClient;

    private final InventoryFeignClient inventoryFeignClient;

    @CircuitBreaker(name="inventoryservice")
    @Retry(name="inventoryservice", fallbackMethod = "fallback")
    public Order saveOrder(Order order){
        // update the inventory with the order
        //call the post method on the inventory service
        log.info("Invoking the inventory REST endpoint from Order Microservice :: ");
        //1st approach
        //invokeInventoryServiceURIWithDiscoverClient();
        //2nd approach
        //invokeUsingClientSideLoadBalancer();
        //3rd approach
       // final ResponseEntity<Integer> response = this.inventoryFeignClient.updateInventory();
        //log.info(" Response from Inventory service :: {}", response);

        //4. using spring cloud stream
        log.info("Pushing the order message to the topic");
        this.outputChannels.outputOrdersTopicChannel1().send(MessageBuilder.withPayload(order).build());
        this.outputChannels.outputOrdersTopicChannel2().send(MessageBuilder.withPayload(order).build());
        return this.orderRepository.save(order);
    }

    private void invokeUsingClientSideLoadBalancer() {
        final ResponseEntity<Integer> integerResponseEntity = this.restTemplate.postForEntity("http://INVENTORYSERVICE/api/inventory", null, Integer.class);
    }

    private Order fallback(Exception exception){
        log.error("Exception while updating the inventory :: {}", exception.getMessage());
        return Order.builder().customerEmail("email").date(LocalDate.now()).build();
    }

    private void invokeInventoryServiceURIWithDiscoverClient(){
        final List<ServiceInstance> inventoryserviceInstances = this.discoveryClient.getInstances("INVENTORYSERVICE");
        inventoryserviceInstances.stream().forEach(inventoryserviceInstance -> log.info(" URI :: {}", inventoryserviceInstance.getUri().toString()));
        final ServiceInstance inventoryServiceInstance = inventoryserviceInstances.get(0);
        String uri = inventoryServiceInstance.getUri().toString();
        uri = uri+"/api/inventory";
        log.info("URI :: {}", uri);
        final ResponseEntity<Integer> integerResponseEntity = this.restTemplate.postForEntity(uri, null, Integer.class);
    }

    public Map<String, Object> fetchOrders(int pageNo, int numberOfRecords, String sortField){
        log.info("Fetching the orders from the db ...");
        Pageable pageRequest = PageRequest.of(pageNo, numberOfRecords, Sort.by(sortField));
        val pageResponse = this.orderRepository.findAll(pageRequest);
        val total = pageResponse.getTotalElements();
        val data = pageResponse.getContent();
        val totalPages = pageResponse.getTotalPages();

        Map<String, Object> response = new LinkedHashMap<>();
        response.put("total_pages", totalPages);
        response.put("elements", total);
        response.put("data", data);
        return response;
    }

    public Order fetchOrderById(long orderId){
        return this.orderRepository.findById(orderId)
                                   .orElseThrow(() -> new IllegalArgumentException("Invalid order Id"));
    }
    public void deleteOrderById(long orderId){
        this.orderRepository.deleteById(orderId);
    }
}