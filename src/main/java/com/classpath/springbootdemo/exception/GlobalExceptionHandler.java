package com.classpath.springbootdemo.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.validation.ConstraintViolationException;
import java.util.ArrayList;
import java.util.List;

@Component
@RestControllerAdvice
@RequiredArgsConstructor
@Slf4j
public class GlobalExceptionHandler {

    private final Environment environment;

    @ExceptionHandler(IllegalArgumentException.class)
    public ResponseEntity<Error> handleInvalidOrderId(IllegalArgumentException illegalArgumentException){
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new Error(111, illegalArgumentException.getMessage()));
    }


    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<Error> constraintsVioaliation(MethodArgumentNotValidException constraintViolationException){
        log.info("Environemt value :: "+ this.environment.getProperty("appname"));
        final List<ObjectError> allErrors = constraintViolationException.getAllErrors();
        List<String> errors = new ArrayList<>();
        allErrors.forEach(error -> errors.add(error.getDefaultMessage()) );
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new Error(200,errors.toString()));
    }
}

@AllArgsConstructor
@Setter
@Getter
class Error {
    private final int code;
    private final String message;
}