package com.classpath.springbootdemo.config;

import com.classpath.springbootdemo.repository.OrderRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.HealthIndicator;
import org.springframework.boot.actuate.health.Status;
import org.springframework.context.annotation.Configuration;

@Configuration
@RequiredArgsConstructor
@Slf4j
class H2DBHealthIndication implements HealthIndicator {

    private final OrderRepository orderRepository;

    @Override
    public Health health() {
      log.info("Verifying the DB connection :: ");
      long count = this.orderRepository.count();
      if (count <=  0 ){
          return Health.status(Status.DOWN).withDetail("H2-DB", "DB service is down").build();
      }
      return Health.up().withDetail("H2-DB", "DB service is up").build();
    }
}

@Configuration
@RequiredArgsConstructor
@Slf4j
class KafkaHealthIndication implements HealthIndicator {
    @Override
    public Health health() {
      return Health.up().withDetail("Kafka-Endpoint", "Kafka service is up").build();
    }
}