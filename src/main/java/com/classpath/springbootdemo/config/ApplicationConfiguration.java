package com.classpath.springbootdemo.config;

import com.github.javafaker.Faker;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;
import org.springframework.context.annotation.Profile;
import org.springframework.web.client.RestTemplate;

@Configuration
public class ApplicationConfiguration {

    @Bean
    @Profile("dev")
    public Faker faker(){
        return  new Faker();
    }

    @Bean
    @LoadBalanced
    public RestTemplate restTemplate(){
        return new RestTemplate();
    }
/*
    @ConditionalOnMissingBean(name = "dataSource")
    @Bean
    public TransactionManager transactionManager(){
        return new TransactionManager();
    }


    @Bean
    @ConditionalOnProperty(prefix = "app", name = "loadConfiguration", havingValue = "true")
    public DataSource dataSource(){
        return new DataSource();
    }

    @Bean
    @ConditionalOnJava(JavaVersion.EIGHT)
    public Java8ThreadPool java8ThreadPool(){
        return new Java8ThreadPool();
    }

    @Bean
    @ConditionalOnJava(JavaVersion.FOURTEEN)
    public Java9ForkJoinThreadPool java9ThreadPool(){
        return new Java9ForkJoinThreadPool();
    }

*/

}

/*
class DataSource {
}
class  TransactionManager {
}

class Java8ThreadPool{}

class Java9ForkJoinThreadPool{}
*/
