package com.classpath.springbootdemo.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

@Component
public class HelloWorldApplication implements CommandLineRunner {

    @Autowired
    private ApplicationContext applicationContext;

    @Override
    public void run(String... args) throws Exception {
        System.out.println("==== Printing the beans ===");
        final String[] beanDefinitionNames = applicationContext.getBeanDefinitionNames();
        /*Arrays.asList(beanDefinitionNames)
                .stream()
                .forEach(System.out::println);*/
    }
}