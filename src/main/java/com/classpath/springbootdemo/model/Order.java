package com.classpath.springbootdemo.model;

import lombok.*;
import javax.persistence.*;
import javax.validation.constraints.*;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;
import static javax.persistence.GenerationType.AUTO;

@Entity
@Table(name="orders")
@Builder
@NoArgsConstructor
@Data
@AllArgsConstructor
@EqualsAndHashCode(exclude = "lineItems")
@ToString
public class Order {
    @Id
    @GeneratedValue(strategy = AUTO)
    private long id;

    @Min(value = 25000, message = "Min order price cannot be less than 25k")
    @Max(value = 80000, message = "Max order price cannot be greater than 80k")
    private double price;

    @PastOrPresent(message = "order date cannot be in future")
    private LocalDate date;

    @Email(message = "not a valid email address")
    private String customerEmail;

    @OneToMany(mappedBy = "order", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Set<LineItem> lineItems;

    public void addLineItem(LineItem lineItem){
        if (this.lineItems == null){
            this.lineItems = new HashSet<>();
        }
        this.lineItems.add(lineItem);
        lineItem.setOrder(this);
    }
}