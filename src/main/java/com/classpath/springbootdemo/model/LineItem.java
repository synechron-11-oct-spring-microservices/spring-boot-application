package com.classpath.springbootdemo.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import javax.persistence.*;
import static javax.persistence.GenerationType.AUTO;

@Entity
@Table(name = "line_items")
@Data
@AllArgsConstructor
@Builder
@NoArgsConstructor
@EqualsAndHashCode(exclude = {"order"})
@ToString(exclude = "order")
public class LineItem {
    @Id
    @GeneratedValue(strategy = AUTO)
    private int id;
    private int qty;

    private String name;

    private double price;

    @ManyToOne
    @JoinColumn(name="order_id_fk", nullable = false)
    @JsonIgnore
    private Order order;

}