package com.classpath.springbootdemo.repository;

import com.classpath.springbootdemo.model.Order;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public interface OrderRepository extends JpaRepository<Order, Long> {

    List<Order> findByCustomerEmail(String emailAddress);

    List<Order> findByPriceGreaterThanEqual(double price);

    Page<Order> findByPriceGreaterThanEqual(double price, Pageable pageable);

    @Query("select order from Order order where order.customerEmail = ?1")
    List<Order> findByEmailAddress(String emailAddress, int offset);

}