# base image
FROM openjdk:11 as builder

# dependencies

WORKDIR /app

COPY mvnw .

COPY .mvn .mvn

COPY pom.xml .

RUN chmod +x ./mvnw

RUN ./mvnw -B dependency:go-offline

# copy the source code and build the docker image

COPY src src

# the below command will generate the target directory under /app/target
RUN ./mvnw package

RUN mkdir /app/target/dependency && (cd /app/target/dependency; jar -xf ../*.jar)

# new image which is build on JRE
FROM openjdk:11.0.12-jre-slim-buster as stage


ARG DEPENDENCY=/app/target/dependency

COPY --from=builder ${DEPENDENCY}/BOOT-INF/lib /app/lib
COPY --from=builder ${DEPENDENCY}/BOOT-INF/classes /app
COPY --from=builder ${DEPENDENCY}/META-INF /app/META-INF

EXPOSE 8222

ENTRYPOINT ["java" , "-cp" ,"app:app/lib/*", "com.classpath.springbootdemo.SpringBootDemoApplication"]








